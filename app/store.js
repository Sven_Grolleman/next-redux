// import { configureStore } from '@reduxjs/toolkit'
// import usersReducer from '../features/usersSlice';
// import {createWrapper} from 'next-redux-wrapper';
//
// const makeStore = () =>
//     configureStore({
//         reducer: {
//             users: usersReducer
//         },
//         devTools: true
//     });
//
// export const wrapper = createWrapper(makeStore);

import {configureStore} from '@reduxjs/toolkit';
import postsReducer from '../features/postsSlice';
import {createWrapper} from 'next-redux-wrapper';

const makeStore = () =>
    configureStore({
        reducer: {
            posts: postsReducer
        },
        devTools: true,
    });


export const wrapper = createWrapper(makeStore);