import '../styles/globals.css'
import {wrapper} from '../app/store';

const App = ({Component, pageProps}) => <Component {...pageProps} />;

export default wrapper.withRedux(App);