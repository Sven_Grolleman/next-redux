import {axiosInstance} from '../../api';

export default function User({user}){
    return(
        <>
            {user.name}
        </>
    )
}

export async function getStaticProps({params}) {
    const {data} = await axiosInstance.get(`/users/${params.id}`)
    return {
        props: {user: data},
        revalidate: 10,
    }
}

export async function getStaticPaths() {
    const {data} = await axiosInstance.get('/users');
    const paths = data.map(user =>{
        console.log(user);
        return {
            params: {id: user.id.toString()}
        }
    })
    return{
        paths,
        fallback: false
    }

}