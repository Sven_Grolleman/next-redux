import UserList from '../../components/users/userList';
import {axiosInstance, fetcher, useUsers} from '../../api';
import useSWR, {SWRConfig} from 'swr';

export default function UsersList({fallback}) {

    return(
        <SWRConfig value={{fallback}}>
            <UserList/>
        </SWRConfig>
    )
}

export async function getStaticProps(){
    const {data} = await axiosInstance.get('/users');
    return {
        props:{
            fallback:{
                '/users' : data
            }
        }
    }
}


