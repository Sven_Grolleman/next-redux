import {Component} from 'react';
import {connect} from 'react-redux';
import {fetchPosts} from '../../features/postsSlice';
import {wrapper} from '../../app/store';
import Link from 'next/link'


class Posts extends Component {
    componentDidMount() {
        this.props.fetchPosts();
    }

    render() {
        return (
            <div>
                {this.renderPosts()}
            </div>
        );
    }

    renderPosts() {
        return this.props.posts.map(post=>{
            return(
                <li key={post.id}>
                    <Link href={`/posts/${post.id}`}>
                        <a >{post.name}</a>
                    </Link>
                </li>
            )
        })
    }
}

function mapStateToProps(state){
    return {posts: state.posts.posts}
}

export const getStaticProps = wrapper.getStaticProps(store=> async ()=>{
    await store.dispatch(fetchPosts());
})

export default connect(mapStateToProps, {fetchPosts})(Posts)
