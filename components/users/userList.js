import React from 'react';
import useSWR from 'swr';
import {fetcher} from '../../api';
import Link from 'next/link';

const UserList = () => {
    const {data} = useSWR('/users', fetcher);

    function renderUserList() {
        if (!data) return <>Loading...</>

        return data.map(user => {
            return (
                <li key={user.id}>
                    <Link  href={`/users/${user.id}`}>
                        <a>{user.name}</a>
                    </Link>
                </li>
            )
        })
    }

    return (
        <div>
            <ul>
                {renderUserList()}
            </ul>
        </div>
    );
};

export default UserList;