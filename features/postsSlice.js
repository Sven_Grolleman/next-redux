import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {axiosInstance} from '../api';
import {HYDRATE} from 'next-redux-wrapper';

export const fetchPosts = createAsyncThunk(
    'posts/fetchPosts',
    async ()=>{
        const res =  await axiosInstance.get('/posts')
        return res.data;
    }
)


const initialState = {

}
export const postsSlice= createSlice({
    name: 'posts',
    initialState,
    reducers: {

    },
    extraReducers:(builder)=>{
        builder.addCase(fetchPosts.fulfilled, (state, action)=>{
            state.posts = action.payload
        });
        builder.addCase(HYDRATE, (state, action) => {
            return{ ...state, ...action.payload.posts}
        });

    }
})

export default postsSlice.reducer;