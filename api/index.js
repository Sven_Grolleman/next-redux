import axios from 'axios';
import useSWR from 'swr';

export const axiosInstance = axios.create({
    baseURL: 'http://localhost:3001'
})

export const fetcher = url => axiosInstance.get(url).then(res => res.data);

export function useUsers(){
    const {data, error} = useSWR('/users', fetcher);
    return{
        data, error
    }
}

export function useUser(id){
    const {data, error} = useSWR(`/users/${id}`, fetcher);
    return{
        data, error
    }
}