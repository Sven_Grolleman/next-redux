export const FETCH_POSTS = 'FETCH_POSTS';
export const fetchPosts = () => async (dispatch, getState, api) => {
    const res = await api.get('/posts');
    dispatch({
       type: FETCH_POSTS,
       payload: res
    });
}